This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for Accounting Library

## [v4.2.0-SNAPSHOT]

- Added compatibility between smartgears 3 and smartgears 4 component


## [v4.1.0] 

- Deprecated not needed properties to reduce data and improve aggregation
- Improved regex to comply with new version of Thredds called methods [#18053]


## [v4.0.0] [r.5.0.0] - 

- Switched JSON management to gcube-jackson [#19115]
- Properly terminated RegexRulesAggregator scheduled thread [#18547]
- Added improved version of calledMethod rewrite for aggregation [#10645]


## [v3.5.0] - 2019-11-06

- Removed usage of deprecated APIs


## [v3.4.0] [r4.12.1] - 2018-10-10

- Fixed backends factory management
- Using different pools for Scheduled Thread [#12184]


## [v3.3.0] [r4.11.0] - 2018-04-12

- Added support to retrieve Accounting Regex rules through Service Endpoint [#11230]
- Added LOCAL as additional value of DataType


## [v3.2.0] [r4.10.0] - 2018-02-15

- Changed pom.xml to use new make-servicearchive directive [#10142]
- Added the possibility to rewrite the calledMethod by matching a regular expression [#10646]


## [v3.1.0] [r4.7.0] - 2017-12-06

- Refined Jackson support [#9617]
- Changed JobUsageRecord Model [#9646]
- Removed TaskUsageRecord [#9647]


## [v3.0.0] [r4.5.0] - 2017-06-07

- Added Jackson support on Usage Record model to allow to use it for marshalling and unmarshalling


## [v2.4.1] [r4.4.0] - 2017-05-02

- Added shutdown() method to pass to document store lib [#7345]


## [v2.4.0] [r4.3.0] - 2017-03-16

- Added new Usage Record (StorageStatusRecord) [#5789]


## [v2.3.0] [r4.1.0] - 2016-11-07

- Added callerQualifier field to ServiceUsageRecord [#4949]


## [v2.2.0] [r3.11.0] - 2016-05-18

- Removed dependency over reflection library [#2358]


## [v2.1.0] [r3.10.1] - 2016-04-08

- Ready for ScopeProvider Removal [#2201]
- Fixed Bug on StorageUsageRecord aggregation [#2316]
- Fixed Bug on recovery of UsageRecords accounted on Fallback [#2437]


## [v2.0.0] [r3.10.0] - 2016-02-08

- Separated Accounting Model and generalize solution. The library now depends from document-store-lib [#1746]
- UsageRecords discovered dynamically on classpath [#1295]
- Persistence is re-checked every 10 minutes when Fallback is used [#1349]
- Repetitive thread retry to persist UsageRecords accounted on Fallback [#1352]


## [v1.1.0] [r3.9.0] - 2015-12-09

- Changed Single and Aggregated classes names to avoid mistakes for the developers [#436]


## [v1.0.0] - 2015-10-01

- First Release [#200]

