package org.gcube.accounting.datamodel.validations.validators;

public class Replace {
	
	protected String serviceClass;
	protected String serviceName; 
	protected String calledMethod;
	
	public Replace() {
		
	}
	
	public Replace(String serviceClass, String serviceName, String calledMethod) {
		this.serviceClass = serviceClass;
		this.serviceName = serviceName;
		this.calledMethod = calledMethod;
	}

	public String getServiceClass() {
		return serviceClass;
	}

	public void setServiceClass(String serviceClass) {
		this.serviceClass = serviceClass;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getCalledMethod() {
		return calledMethod;
	}

	public void setCalledMethod(String calledMethod) {
		this.calledMethod = calledMethod;
	}
	
	@Override
	public String toString() {
		return "Replacer [serviceClass=" + serviceClass + ", serviceName=" + serviceName + ", calledMethod="
				+ calledMethod + "]";
	}
	
}