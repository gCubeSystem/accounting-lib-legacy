package org.gcube.accounting.datamodel.validations.validators;

import java.util.regex.Pattern;

import org.gcube.com.fasterxml.jackson.annotation.JsonIgnore;

public class MultiMatcher {
	
	protected String serviceClassRegex;
	@JsonIgnore
	protected Pattern serviceClassPattern;
	
	protected String serviceNameRegex;
	@JsonIgnore
	protected Pattern serviceNamePattern;
	
	protected String calledMethodRegex;
	@JsonIgnore
	protected Pattern calledMethodPattern;
	
	protected MultiMatcher() {}
	
	public MultiMatcher(String serviceClassRegex, String serviceNameRegex, String calledMethodRegex) {
		setServiceClassRegex(serviceClassRegex);
		setServiceNameRegex(serviceNameRegex);
		setCalledMethodRegex(calledMethodRegex);
	}
	
	protected Pattern getPattern(String regex) {
		return Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
	}

	public String getServiceClassRegex() {
		return serviceClassRegex;
	}
	
	public void setServiceClassRegex(String serviceClassRegex) {
		this.serviceClassRegex = serviceClassRegex;
		this.serviceClassPattern = getPattern(serviceClassRegex);
	}

	public String getServiceNameRegex() {
		return serviceNameRegex;
	}
	
	public void setServiceNameRegex(String serviceNameRegex) {
		this.serviceNameRegex = serviceNameRegex;
		this.serviceNamePattern = getPattern(serviceNameRegex);
	}

	public String getCalledMethodRegex() {
		return calledMethodRegex;
	}
	
	public void setCalledMethodRegex(String calledMethodRegex) {
		this.calledMethodRegex = calledMethodRegex;
		this.calledMethodPattern = getPattern(calledMethodRegex);
	}
	
	@JsonIgnore
	public boolean match(String serviceClass, String serviceName, String calledMethod) {
		return serviceClassPattern.matcher(serviceClass).matches() && 
				serviceNamePattern.matcher(serviceName).matches() &&
				calledMethodPattern.matcher(calledMethod).matches();
	}

	@Override
	public String toString() {
		return "MultiMatcher [serviceClassRegex=" + serviceClassRegex + ", serviceNameRegex=" + serviceNameRegex
				+ ", calledMethodRegex=" + calledMethodRegex + "]";
	}

	public Pattern getServiceClassPattern() {
		return serviceClassPattern;
	}

	public Pattern getServiceNamePattern() {
		return serviceNamePattern;
	}

	public Pattern getCalledMethodPattern() {
		return calledMethodPattern;
	}
	
}