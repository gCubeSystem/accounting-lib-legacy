#!/bin/bash
echo "[" > allRules.json.full
cat ./*.json >> allRules.json.full
cat **/*.json >> allRules.json.full
echo "]" >> allRules.json.full
sed -i 's/}{/},{/g' allRules.json.full


tr -d '\n' < allRules.json.full > allRules.json.min
sed -i 's/\t//g' allRules.json.min
sed -i 's/  / /g' allRules.json.min
sed -i 's/": "/":"/g' allRules.json.min
sed -i 's/": {/":{/g' allRules.json.min